from django.db import models
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager


class Speaker(models.Model):
    picture = models.ImageField(
        upload_to=u'speakers/',
        blank=True,
        null=True
    )
    name = models.CharField(
        max_length=100
    )
    reference = models.CharField(
        max_length=100
    )
    about = models.TextField(
        max_length=200,
        blank=True,
        null=True
    )
    facebook = models.URLField(
        blank=True,
        null=True
    )
    twitter = models.URLField(
        blank=True,
        null=True
    )
    googleplus = models.URLField(
        blank=True,
        null=True
    )
    homepage = models.URLField(
        blank=True,
        null=True
    )
    sites = models.ManyToManyField(Site)

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.name

    def schedules(self):
        return Schedule.on_site.filter(speaker=self).order_by('start_at')


class Schedule(models.Model):
    start_at = models.TimeField()
    title = models.CharField(
        max_length=100
    )
    about = models.TextField(
        max_length=200,
        blank=True,
        null=True
    )
    speaker = models.ForeignKey(Speaker)
    site = models.ForeignKey(Site)

    objects = models.Manager()
    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.title

QUOTE_CHOICES = (
    ('diamond', 'Diamond'),
    ('platinum', 'Platinum'),
    ('gold', 'Gold'),
    ('support', 'Support'),
)


class Partner(models.Model):
    sm_image = models.ImageField(
        upload_to='partners/lower/',
        help_text=u'High resolution with 110x35.'
    )
    lg_image = models.ImageField(
        upload_to='partners/higher/',
        help_text=u'High resolution with 320x160.'
    )
    name = models.CharField(
        max_length=100
    )
    homepage = models.URLField(
        blank=True,
        null=True
    )
    about = models.TextField(
        max_length=200,
        blank=True,
        null=True
    )
    quote = models.CharField(
        max_length=25,
        default='gold',
        choices=QUOTE_CHOICES
    )
    site = models.ForeignKey(Site)

    on_site = CurrentSiteManager()

    def __unicode__(self):
        return self.name


CITY_CHOICES = (
    ('bh', 'Belo Horizonte'),
)


class Conference(models.Model):
    realization_at = models.DateField(
        blank=True,
        null=True
    )
    address = models.CharField(
        max_length=180,
        blank=True,
        null=True
    )
    city = models.CharField(
        max_length=2,
        choices=CITY_CHOICES,
        default='bh'
    )
    latitude = models.CharField(
        max_length=30,
        blank=True,
        null=True
    )
    longitude = models.CharField(
        max_length=30,
        blank=True,
        null=True
    )

    site = models.OneToOneField(Site)

    def __unicode__(self):
        return self.site.name
