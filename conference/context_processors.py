from django.contrib.sites.models import Site

from .models import Conference


def conference_current(request):
    current_site = Site.objects.get_current()
    obj, created = Conference.objects.get_or_create(site=current_site)
    return {
        'conference': obj
    }
