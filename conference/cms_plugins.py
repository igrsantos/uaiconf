from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from core.helpers import chunks

from .models import Schedule, Speaker, Partner


class SchedulePlugin(CMSPluginBase):
    name = _("Schedule Plugin")
    render_template = "plugins/conference/schedule.html"
    cache = False

    def render(self, context, instance, placeholder):
        queryset = Schedule.on_site.order_by('start_at')
        context.update({
            'instance': instance,
            'schedule_list': queryset,
        })
        return context

plugin_pool.register_plugin(SchedulePlugin)


class SpeakerPlugin(CMSPluginBase):
    name = _("Speaker Plugin")
    render_template = "plugins/conference/speakers.html"
    cache = False

    def render(self, context, instance, placeholder):
        queryset = Speaker.on_site.all()
        context.update({
            'instance': instance,
            'speakers_list': chunks(queryset, 4),
        })
        return context

plugin_pool.register_plugin(SpeakerPlugin)


class PartnerPlugin(CMSPluginBase):
    name = _("Partner Plugin")
    render_template = "plugins/conference/partners.html"
    cache = False

    def render(self, context, instance, placeholder):
        partners_list = Partner.on_site.exclude(quote='support')
        support_list = Partner.on_site.filter(quote='support')
        context.update({
            'instance': instance,
            'partners_list': partners_list,
            'support_list': support_list,
        })
        return context

plugin_pool.register_plugin(PartnerPlugin)
