# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conference', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='speaker',
            name='picture',
            field=models.ImageField(null=True, upload_to='speakers/', blank=True),
            preserve_default=True,
        ),
    ]
