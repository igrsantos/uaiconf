# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('conference', '0009_auto_20150511_2052'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='partner',
            name='sites',
        ),
        migrations.AddField(
            model_name='partner',
            name='site',
            field=models.ForeignKey(default=1, to='sites.Site'),
            preserve_default=False,
        ),
    ]
