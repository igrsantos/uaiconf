# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conference', '0007_auto_20150507_0251'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='city',
            field=models.CharField(default=b'bh', max_length=2, choices=[(b'bh', b'Belo Horizonte')]),
            preserve_default=True,
        ),
    ]
