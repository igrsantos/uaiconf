# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conference', '0004_conference'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='conference',
            name='realization_date',
        ),
        migrations.AddField(
            model_name='conference',
            name='realization_at',
            field=models.DateField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
