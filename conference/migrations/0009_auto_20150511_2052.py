# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conference', '0008_conference_city'),
    ]

    operations = [
        migrations.RenameField(
            model_name='partner',
            old_name='hompage',
            new_name='homepage',
        ),
        migrations.AddField(
            model_name='partner',
            name='quote',
            field=models.CharField(default=b'gold', max_length=25, choices=[(b'diamond', b'Diamond'), (b'platinum', b'Platinum'), (b'gold', b'Gold'), (b'support', b'Support')]),
            preserve_default=True,
        ),
    ]
