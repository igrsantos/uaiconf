# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Partner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sm_image', models.ImageField(help_text='High resolution with 110x35.', upload_to=b'partners/lower/')),
                ('lg_image', models.ImageField(help_text='High resolution with 320x160.', upload_to=b'partners/higher/')),
                ('name', models.CharField(max_length=100)),
                ('hompage', models.URLField(null=True, blank=True)),
                ('about', models.TextField(max_length=200, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_at', models.TimeField()),
                ('title', models.CharField(max_length=100)),
                ('about', models.TextField(max_length=200, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Speaker',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('picture', models.ImageField(upload_to='speakers/')),
                ('name', models.CharField(max_length=100)),
                ('reference', models.CharField(max_length=100)),
                ('about', models.TextField(max_length=200, null=True, blank=True)),
                ('facebook', models.URLField(null=True, blank=True)),
                ('twitter', models.URLField(null=True, blank=True)),
                ('googleplus', models.URLField(null=True, blank=True)),
                ('homepage', models.URLField(null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='schedule',
            name='speaker',
            field=models.ForeignKey(to='conference.Speaker'),
            preserve_default=True,
        ),
    ]
