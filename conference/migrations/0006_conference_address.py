# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('conference', '0005_auto_20150506_2133'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='address',
            field=models.CharField(max_length=180, null=True, blank=True),
            preserve_default=True,
        ),
    ]
