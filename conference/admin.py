from django.contrib import admin

from .models import Schedule, Speaker, Partner, Conference


admin.site.register(Schedule)

admin.site.register(Speaker)

admin.site.register(Partner)


class ConferenceAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('realization_at', )
        }),
        ('Address', {
            'fields': (
                ('address', 'city'),
                ('latitude', 'longitude'),
            )
        }),
        ('Site', {
            'fields': ('site', )
        }),
    )

admin.site.register(Conference, ConferenceAdmin)
