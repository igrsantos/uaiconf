from django.db import models

from cms.models.pluginmodel import CMSPlugin


class PageContent(CMSPlugin):
    background = models.BooleanField(
        default=False
    )
    identify = models.CharField(
        max_length=100
    )
    published = models.BooleanField(
        default=False
    )

    def __unicode__(self):
        return self.identify


class Anchor(CMSPlugin):
    name = models.CharField(
        max_length=80
    )
    identify = models.CharField(
        max_length=100
    )

    def __unicode__(self):
        return self.name


class SlideShow(CMSPlugin):
    name = models.CharField(
        max_length=100
    )

    def __unicode__(self):
        return self.name

    def copy_relations(self, oldinstance):
        for associated_item in oldinstance.slide_set.all():
            associated_item.pk = None
            associated_item.plugin = self
            associated_item.save()


class Slide(models.Model):
    title = models.CharField(
        max_length=100
    )
    image = models.ImageField(
        upload_to='slides/'
    )
    plugin = models.ForeignKey(SlideShow)

    def __unicode__(self):
        return self.title


class About(CMSPlugin):
    body = models.TextField()
    media = models.TextField(
        blank=True,
        null=True,
        help_text=u'Media to publicize the event'
    )

    def __unicode__(self):
        return 'About <{}>'.format(self.pk)
