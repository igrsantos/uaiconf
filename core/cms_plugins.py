from django.utils.translation import ugettext_lazy as _

from adminsortable.admin import SortableAdmin
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from .admin import SlideInline
from .models import Anchor, PageContent, SlideShow, About


class PageContentPlugin(CMSPluginBase):
    model = PageContent
    name = _("Page Content Plugin")
    render_template = "plugins/page_content.html"
    allow_children = True

plugin_pool.register_plugin(PageContentPlugin)


class AnchorPlugin(CMSPluginBase):
    model = Anchor
    name = _("Anchor Plugin")
    render_template = "plugins/anchor.html"

plugin_pool.register_plugin(AnchorPlugin)


class SlideshowPlugin(SortableAdmin, CMSPluginBase):
    model = SlideShow
    name = _("SlideShow Plugin")
    render_template = "plugins/slides.html"
    inlines = (SlideInline, )

plugin_pool.register_plugin(SlideshowPlugin)


class AboutPlugin(CMSPluginBase):
    model = About
    name = _("About Plugin")
    render_template = "plugins/about.html"

plugin_pool.register_plugin(AboutPlugin)
