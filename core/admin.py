from django.contrib.admin import StackedInline

from .models import Slide


class SlideInline(StackedInline):
    model = Slide
    extra = 1
