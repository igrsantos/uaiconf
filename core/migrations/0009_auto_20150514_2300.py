# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_pagecontent_published'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='slide',
            options={},
        ),
        migrations.RemoveField(
            model_name='slide',
            name='order',
        ),
    ]
