# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_remove_about_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='pagecontent',
            name='published',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
