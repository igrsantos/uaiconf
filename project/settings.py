"""
Django settings for project project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""
from django.conf import global_settings

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#cw!z1s0_bqfiy_saf_d*u^pjo^$p^yh(rks#3dr^-2zscbr93'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

THUMBNAIL_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'djangocms_admin_style',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'cms',
    'adminsortable',
    'treebeard',
    'menus',
    'sekizai',
    'sorl.thumbnail',

    'core',
    'conference',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    'django.core.context_processors.request',
    'django.core.context_processors.static',

    'sekizai.context_processors.sekizai',
    'cms.context_processors.cms_settings',

    'conference.context_processors.conference_current',
)

SITE_ID = 1

ROOT_URLCONF = 'project.urls'

WSGI_APPLICATION = 'project.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

#MIGRATION_MODULES = {
#    # Add also the following modules if you're using these plugins:
#    'djangocms_file': 'djangocms_file.migrations_django',
#    'djangocms_flash': 'djangocms_flash.migrations_django',
#    'djangocms_googlemap': 'djangocms_googlemap.migrations_django',
#    'djangocms_inherit': 'djangocms_inherit.migrations_django',
#    'djangocms_link': 'djangocms_link.migrations_django',
#    'djangocms_picture': 'djangocms_picture.migrations_django',
#    'djangocms_snippet': 'djangocms_snippet.migrations_django',
#    'djangocms_teaser': 'djangocms_teaser.migrations_django',
#    'djangocms_video': 'djangocms_video.migrations_django',
#    'djangocms_text_ckeditor': 'djangocms_text_ckeditor.migrations_django',
#}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LANGUAGES = (
    ('en-us', 'English'),
)


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_ROOT = '/tmp/uaipython/static/'

STATIC_URL = '/static/'

MEDIA_ROOT = '/tmp/uaipython/media/'

MEDIA_URL = '/media/'


TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

CMS_TEMPLATES = (
    ('base.html', 'Template Base'),
)


try:
    from local_settings import *
except ImportError:
    pass
